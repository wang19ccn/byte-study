import { DatePicker } from 'antd';
import 'antd/dist/antd.css';
import './App.css';
function App() {
  return (
    <div className="App">
      <h1><a id="logo"><img alt="logo" src="logo.svg" /> Ant Design</a></h1>
    </div>
  );
}

export default App;
