// getting-started.js
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/fruit', { useNewUrlParser: true, useUnifiedTopology: true });

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

var fruitSchema = mongoose.Schema({
    name: String,
    price: Number,
    num: Number
});

var Fruit = mongoose.model('fruit', fruitSchema);

const addFruit = ({ name, price, num }) => {
    const fruit = new Fruit({
        name,
        price,
        num
    })
    return fruit.save()
}

console.log(addFruit({ name: "苹果", price: 10, num: 100 }))
console.log(addFruit({ name: "香蕉", price: 5, num: 200 }))