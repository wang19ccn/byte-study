const fs = require('fs')
const http = require('http')
http.createServer((request, response) => {
    const { url, method } = request
    if (url === '/' && method === 'GET') {
        fs.readFile('http.html', (err, data) => {
            if (err) {
                response.writeHead(500, {
                    'Content-Type': 'text/plain;charset=utf-8'
                })
                response.end('500 服务器挂了')
                return
            }
            response.statusCode = 200
            response.setHeader('Content-type', 'text-html')
            response.end(data)
        })
    } else {
        response.statusCode = 404
        response.setHeader('Content-type', 'text/plain;charset=utf-8')
        response.end('404 没有这玩意')
    }
}).listen(3000, () => {
    console.log('Serve at 3000')
})
